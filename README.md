# Api

A framework designed for building apis using the [Symfony HttpKernel](http://symfony.com/doc/current/components/http_kernel/introduction.html#component-http-kernel-event-table)

## Installation

Installation via composer:

```json
{
    "require": {
        "krak/api": "^0.6"
    }
}
```

## Event Listener

The Event Listener holds an option ViewListener that will take the result of your controllers and use an http converter to convert them into a proper response.

## HttpConverter

Converts and formats an Api Response into a [HTTP Response](http://symfony.com/doc/current/components/http_foundation/introduction.html) in the proper format.

## Response

A response is the vehicle for your api data. The Response holds the status type and builds an extra headers or status information around the data.

## ResponseMarshaler

Response Marshalers take a response and create the array data to be converted to an Http response.

### Usage

ResponseMarshalers are attached to HttpConverters.

```php
use Krak\Api\HttpConverter\JsonHttpConverter;
use Krak\Api\ResponseMarshaler\StatusResponseMarshaler;

$converter = new JsonHttpConverter();
$converter->setResponseMarshaler(new StatusResponseMarshaler());
```

### StatusResponseMarshaler

The status response marshaler will add a status field to the response data

## Todo

- Create marshalers and fix the Response object model

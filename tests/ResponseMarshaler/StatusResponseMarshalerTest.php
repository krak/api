<?php

namespace Krak\Tests\ResponseMarshaler;

use Krak\Api\ResponseMarshaler\StatusResponseMarshaler;
use Krak\Api\Response\OkResponse;
use Krak\Api\Response\Statuses;
use Krak\Tests\TestCase;

class StatusResponseMarshalerTest extends TestCase
{
    public function setUp()
    {}
    
    public function tearDown()
    {}
    
    public function testMarshal()
    {
        $should_equal_data = [
            'status' => Statuses::OK,
            'data' => 'value',
        ];
        
        $marshaler = new StatusResponseMarshaler();
        $data = $marshaler->marshalResponse(new OkResponse('value'));
        
        $this->assertEquals($should_equal_data, $data);
    }
}

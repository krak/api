<?php

namespace Krak\Tests\Marshaler;

use Krak\Api\Marshaler\MockMarshaler,
    Krak\Tests\TestCase;

class MockMarshalerTest extends TestCase
{
    public function setUp()
    {}

    public function tearDown()
    {}

    public function testMarshal()
    {
        $marshaler = new MockMarshaler('test');

        $this->assertEquals('test', $marshaler->marshal('data'));
    }
}

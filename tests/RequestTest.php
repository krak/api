<?php

namespace Krak\Tests;

use Krak\Api,
    Krak\Tests\TestCase,
    Symfony\Component\HttpFoundation\Request;

class RequestTest extends TestCase
{
    private function createRequestWithContent($content)
    {
        $req = new Request([], [], [], [], [], [], json_encode($content));
        $req->headers->set('Content-Type', 'application/json');
        return $req;
    }

    public function testGetJson()
    {
        $req = $this->createRequestWithContent(['key' => 'val']);

        $this->assertEquals(
            ['key'=>'val'],
            api\request_json($req)
        );
    }

    public function testGetJsonNotArray()
    {
        $req = $this->createRequestWithContent(1);

        $this->assertEquals(
            [],
            api\request_json($req)
        );
    }

    public function testGetJsonNoJson()
    {
        $this->assertEquals(
            [],
            api\request_json(new Request())
        );
    }

    public function testGetJsonRaw()
    {
        $req = $this->createRequestWithContent(1);
        $this->assertEquals(
            1,
            api\request_json($req, true)
        );
    }

    public function testGetJsonRawNoJson()
    {
        $this->assertNull(api\request_json(new Request(), true));
    }
}

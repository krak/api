<?php

namespace Krak\Tests\Response;

use Krak\Api\Response\ErrorResponse;
use Krak\Api\Response\Statuses;
use Krak\Tests\TestCase;

class ErrorResponseTest extends TestCase
{
    public function setUp()
    {}

    public function tearDown()
    {}

    public function testStatus()
    {
        $resp = new ErrorResponse([]);
        $this->assertEquals(Statuses::ERROR, $resp->getStatus());
    }

    public function testGetData()
    {
        $resp = new ErrorResponse('value');
        $this->assertEquals('value', $resp->getData());
    }

    public function testErrorData()
    {
        $should_equal_data = [
            'error' => 'value'
        ];

        $resp = new ErrorResponse('value');
        $data = $resp->createResponseData();

        $this->assertEquals($should_equal_data, $data);
    }
}

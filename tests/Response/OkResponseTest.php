<?php

namespace Krak\Tests\Response;

use Krak\Api\Marshaler,
    Krak\Api\Response\OkResponse,
    Krak\Api\Response\Statuses,
    Krak\Tests\TestCase;

class OkResponseTest extends TestCase
{
    public function setUp()
    {}

    public function tearDown()
    {}

    public function testStatus()
    {
        $resp = new OkResponse([]);
        $this->assertEquals(Statuses::OK, $resp->getStatus());
    }

    public function testGetData()
    {
        $resp = new OkResponse('value');
        $this->assertEquals('value', $resp->getData());
    }

    public function testOkData()
    {
        $should_equal_data = [
            'data' => 'value'
        ];

        $resp = new OkResponse('value');
        $data = $resp->createResponseData();

        $this->assertEquals($should_equal_data, $data);
    }

    public function testMarshaledData()
    {
        $should_equal_data = [
            'data' => 'test'
        ];

        $resp = new OkResponse('value', new Marshaler\MockMarshaler('test'));
        $data = $resp->createResponseData();

        $this->assertEquals($should_equal_data, $data);
    }
}

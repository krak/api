<?php

namespace Krak\Tests\Response;

use Krak\Api\Marshaler\MockMarshaler,
    Krak\Api\Response,
    Krak\Tests\TestCase,
    StringTemplate\Engine;

class ResponseFactoryTest extends TestCase
{
    public function testConstruct()
    {
        $factory = new Response\ResponseFactory(
            new Response\ErrorCodeMapping(new Engine(), [])
        );

        $this->assertInstanceOf(Response\ResponseFactory::class, $factory);
        return $factory;
    }

    /**
     * @depends testConstruct
     */
    public function testErrorCode($factory)
    {
        $this->assertInstanceOf(
            Response\ErrorCodeResponse::class,
            $factory->errorCode('code')
        );
    }

    /**
     * @depends testConstruct
     */
    public function testOk($factory)
    {
        $this->assertInstanceOf(
            Response\OkResponse::class,
            $factory->ok([])
        );
    }

    /**
     * @depends testConstruct
     */
    public function testError($factory)
    {
        $this->assertInstanceOf(
            Response\ErrorResponse::class,
            $factory->error('error')
        );
    }
}

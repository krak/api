<?php

namespace Krak\Tests\Response;

use Krak\Api\Response,
    Krak\Tests\TestCase;

class ErrorCodeResponseTest extends TestCase
{
    public function testConstruct()
    {
        $resp = new Response\ErrorCodeResponse('code', 'message');

        $this->assertInstanceOf(Response\ErrorCodeResponse::class, $resp);

        return $resp;
    }

    /**
     * @depends testConstruct
     */
    public function testGetCode($resp)
    {
        $this->assertEquals('code', $resp->getCode());
    }

    /**
     * @depends testConstruct
     */
    public function testGetMessage($resp)
    {
        $this->assertEquals('message', $resp->getMessage());
    }

    /**
     * @depends testConstruct
     */
    public function testGetStatus($resp)
    {
        $this->assertEquals(Response\Statuses::ERROR, $resp->getStatus());
    }

    /**
     * @depends testConstruct
     */
    public function testCreateResponseData($resp)
    {
        $expected = [
            'error' => [
                'code' => 'code',
                'message' => 'message'
            ]
        ];

        $this->assertEquals($expected, $resp->createResponseData());
    }
}

<?php

namespace Krak\Tests\Response;

use Krak\Api\Response,
    Krak\Tests\TestCase,
    StringTemplate\Engine;

class ErrorCodeMappingTest extends TestCase
{
    public function testConstruct()
    {
        $mapping = new Response\ErrorCodeMapping(new Engine(), [
            'code1' => 'message {key}'
        ]);

        $this->assertInstanceOf(Response\ErrorCodeMapping::class, $mapping);

        return $mapping;
    }

    /**
     * @depends testConstruct
     */
    public function testGetErrorMessage($mapping)
    {
        $this->assertEquals(
            'message yo',
            $mapping->getErrorMessage('code1', ['key' => 'yo'])
        );
    }

    /**
     * @depends testConstruct
     */
    public function testGetErrorMessageMissing($mapping)
    {
        $this->assertEquals(
            '',
            $mapping->getErrorMessage('code2', null)
        );
    }
}

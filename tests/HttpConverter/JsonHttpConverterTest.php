<?php

namespace Krak\Tests\HttpConverter;

use Krak\Api\HttpConverter\JsonHttpConverter;
use Krak\Api\Response\OkResponse;
use Krak\Api\ResponseMarshaler\StatusResponseMarshaler;
use Krak\Tests\TestCase;

use Symfony\Component\HttpFoundation\Request;

class JsonHttpConverterTest extends TestCase
{
    public function setUp()
    {}

    public function tearDown()
    {}

    public function testConverter()
    {
        $converter = new JsonHttpConverter();
        $resp = $converter->toHttp(new OkResponse('value'));

        $this->assertEquals('{"data":"value"}', $resp->getContent());
    }

    public function testConverterPrettyPrint()
    {
        $converter = new JsonHttpConverter(true);
        $resp = $converter->toHttp(new OkResponse('value'));

        $json = <<<json
{
    "data": "value"
}
json;

        $this->assertEquals($json, $resp->getContent());
    }

    public function testConverterMarshaler()
    {
        $converter = new JsonHttpConverter();
        $converter->setResponseMarshaler(new StatusResponseMarshaler());
        $resp = $converter->toHttp(new OkResponse('value'));

        $this->assertEquals('{"status":"OK","data":"value"}', $resp->getContent());
    }
}

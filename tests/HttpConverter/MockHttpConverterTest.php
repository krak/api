<?php

namespace Krak\Tests\HttpConverter;

use Krak\Api\HttpConverter\MockHttpConverter;
use Krak\Api\Response\OkResponse;
use Krak\Tests\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MockHttpConverterTest extends TestCase
{
    public function setUp()
    {}

    public function tearDown()
    {}

    public function testGetRequest()
    {
        $m = new MockHttpConverter(new Response());
        $req = new Request();
        $m->setRequest($req);

        $this->assertEquals($req, $m->getRequest());
    }

    public function testToHttp()
    {
        $resp = new Response();
        $m = new MockHttpConverter($resp);

        $this->assertEquals($resp, $m->toHttp(new OkResponse('')));
    }
}

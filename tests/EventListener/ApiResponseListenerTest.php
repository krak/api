<?php

namespace Krak\Tests\EventListener;

use Krak\Api\EventListener\ApiResponseListener,
    Krak\Api\HttpConverter\MockHttpConverter,
    Krak\Api\Response\OkResponse,
    Krak\Tests\TestCase,

    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response,
    Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent as Event,
    Symfony\Component\HttpKernel\HttpKernelInterface;

class ApiResponseListenerTest extends TestCase
{
    private $kernel;

    public function setUp()
    {
        $this->kernel = $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface');
    }

    public function tearDown()
    {
        $this->kernel = null;
    }

    public function testSubscribedEvents()
    {
        $this->assertInternalType('array', ApiResponseListener::getSubscribedEvents());
    }

    public function testKernelViewNotMasterRequest()
    {
        $event = new Event(
            $this->kernel,
            new Request(),
            HttpKernelInterface::SUB_REQUEST,
            new OkResponse('')
        );

        $listener = new ApiResponseListener(new MockHttpConverter(new Response()));
        $listener->onKernelView($event);

        $this->assertNull($event->getResponse());
    }

    public function testKernelViewNotApiResponse()
    {
        $event = new Event(
            $this->kernel,
            new Request(),
            HttpKernelInterface::MASTER_REQUEST,
            []
        );

        $listener = new ApiResponseListener(new MockHttpConverter(new Response()));
        $listener->onKernelView($event);

        $this->assertNull($event->getResponse());
    }

    public function testKernelView()
    {
        $req = new Request();
        $event = new Event(
            $this->kernel,
            $req,
            HttpKernelInterface::MASTER_REQUEST,
            new OkResponse('')
        );

        $resp = new Response();
        $converter = new MockHttpConverter($resp);
        $listener = new ApiResponseListener($converter);
        $listener->onKernelView($event);

        $this->assertEquals($resp, $event->getResponse());
    }
}

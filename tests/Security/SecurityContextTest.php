<?php

namespace Krak\Tests\Security;

use Krak\Api\Security,
    Krak\Tests\TestCase;

class SecurityContextTest extends TestCase
{
    public function testConstruct()
    {
        $tok = new Security\Token\ArrayToken();
        $factory = new Security\Token\MockTokenFactory($tok);
        $context = new Security\SecurityContext($factory);

        $this->assertInstanceOf('Krak\Api\Security\SecurityContext', $context);
        return [$context, $factory, $tok];
    }

    /**
     * @depends testConstruct
     */
    public function testGetTokenFactory($tuple)
    {
        $this->assertSame($tuple[0]->getTokenFactory(), $tuple[1]);
    }

    /**
     * @depends testConstruct
     */
    public function testGetToken($tuple)
    {
        $this->assertSame($tuple[0]->getToken(), $tuple[2]);
    }
}

<?php

namespace Krak\Tests\Security\Firewall;

use Krak\Api\Security\Firewall,
    Krak\Api\Security\Token,
    Krak\Tests\TestCase,
    Symfony\Component\HttpFoundation\Request;

class CollectionFirewallTest extends TestCase
{
    /**
     * @dataProvider firewallProvider
     */
    public function testAllow($firewalls, $res)
    {
        $firewall = new Firewall\CollectionFirewall($firewalls);

        $this->assertEquals(
            $res,
            $firewall->allowRequest(new Request(), new Token\ArrayToken())
        );
    }

    public function firewallProvider()
    {
        return [
            [
                [new Firewall\StaticFirewall(false)],
                false,
            ],
            [
                [new Firewall\StaticFirewall(true)],
                true,
            ]
        ];
    }
}

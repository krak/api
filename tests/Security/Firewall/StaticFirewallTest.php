<?php

namespace Krak\Tests\Security\Firewall;

use Krak\Api\Security\Firewall,
    Krak\Api\Security\Token,
    Krak\Tests\TestCase,
    Symfony\Component\HttpFoundation\Request;

class StaticFirewallTest extends TestCase
{
    public function testAllow()
    {
        $firewall = new Firewall\StaticFirewall(true);
        $this->assertTrue($firewall->allowRequest(new Request(), new Token\ArrayToken()));
    }
}

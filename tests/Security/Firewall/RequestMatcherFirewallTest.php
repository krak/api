<?php

namespace Krak\Tests\Security\Firewall;

use Krak\Api\Security\Firewall,
    Krak\Api\Security\Token,
    Krak\Tests\TestCase,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\RequestMatcherInterface;

class RequestMatcherFirewallTest extends TestCase
{
    public function testConstruct()
    {
        $firewall = new Firewall\RequestMatcherFirewall(
            new StaticRequestMatcher(true),
            []
        );

        $this->assertInstanceOf(
            'Krak\Api\Security\Firewall\RequestMatcherFirewall',
            $firewall
        );
    }

    /**
     * @dataProvider firewallProvider
     */
    public function testAllow(Firewall\RequestMatcherFirewall $firewall, $res)
    {
        $req = new Request();
        $tok = new Token\ArrayToken();
        $tok->addPermission('test');

        $this->assertEquals($res, $firewall->allowRequest($req, $tok));
    }

    public function firewallProvider()
    {
        return [
            [
                new Firewall\RequestMatcherFirewall(
                    new StaticRequestMatcher(false),
                    []
                ),
                true
            ],
            [
                new Firewall\RequestMatcherFirewall(
                    new StaticRequestMatcher(true),
                    ['test-1']
                ),
                false
            ],
            [
                new Firewall\RequestMatcherFirewall(
                    new StaticRequestMatcher(true),
                    ['test']
                ),
                true
            ],
        ];
    }
}

class StaticRequestMatcher implements RequestMatcherInterface
{
    private $match;

    public function __construct($match)
    {
        $this->match = $match;
    }

    public function matches(Request $req) {
        return $this->match;
    }
}

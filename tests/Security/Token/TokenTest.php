<?php

namespace Krak\Tests\Security\Token;

use Krak\Api\Security\Token,
    Krak\Tests\TestCase;

class TokenTest extends TestCase
{
    public function testConstruct()
    {
        $tok = new MockToken(new Token\ArrayToken());
        $this->assertCount(0, $tok->getAllPermissions());
        return $tok;
    }

    /**
     * @depends testConstruct
     */
    public function testAdd(MockToken $tok)
    {
        $tok->addPermission('user');
        $this->assertTrue($tok->hasPermission('user'));

        return $tok;
    }

    /**
     * @depends testAdd
     */
    public function testRevoke(MockToken $tok)
    {
        $tok->revokePermission('user');
        $this->assertFalse($tok->hasPermission('user'));

        return $tok;
    }

    /**
     * @depends testRevoke
     */
    public function testApiClient(MockToken $tok)
    {
        $tok->setApiClientId(1);
        $this->assertEquals($tok->getApiClientId(), 1);

        return $tok;
    }

    /**
     * @depends testApiClient
     */
    public function testUser(MockToken $tok)
    {
        $tok->setUserId(1);
        $this->assertEquals($tok->getUserId(), 1);

        return $tok;
    }
}

class MockToken extends Token\AbstractTokenDecorator implements
    Token\ApiClientToken,
    Token\UserToken
{
    use Token\ApiClientTokenTrait,
        Token\UserTokenTrait;
}

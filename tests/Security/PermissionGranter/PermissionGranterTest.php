<?php

namespace Krak\Tests\Security\PermissionGranter;

use Krak\Api\Security\PermissionGranter,
    Krak\Api\Security\Token,
    Krak\Tests\TestCase;

class PermissionGranterTest extends TestCase
{
    public function testNullPersmissionGranter()
    {
        $granter = new PermissionGranter\NullPermissionGranter();
        $tok = new Token\ArrayToken();
        $granter->grantPermission($tok);
        $this->assertTrue(count($tok->getAllPermissions()) == 0);
    }
}

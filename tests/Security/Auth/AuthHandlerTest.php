<?php

namespace Krak\Tests\Security\Auth;

use Krak\Api\Security\Auth,
    Krak\Api\Security\PermissionGranter,
    Krak\Api\Security\Token,
    Krak\Api\Security,
    Krak\Tests\TestCase,
    Symfony\Component\HttpFoundation\Request;

class AuthHandlerTest extends TestCase
{
    public function testHandle()
    {
        $tok = new Security\Token\ArrayToken();
        $handler = new Security\Auth\AuthRequestHandler(
            new Security\SecurityContext(
                new Security\Token\MockTokenFactory($tok)
            ),
            [
                new Security\Auth\AlwaysAuth(
                    new Security\PermissionGranter\SimplePermissionGranter([
                        'test'
                    ])
                )
            ]
        );

        $handler->handleRequest(new Request());

        $this->assertTrue($tok->hasPermission('test'));
    }
}

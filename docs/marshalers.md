# Marshaler

**NOTE:** this module is deprecated in favor of the [Krak Marshal Library](https://gitlab.bighead.net/krak/marshal)

The marshalers are responsible for transforming data to be presented.

All marshalers implement a simple Marshaler interface

```php
<?php

interface Marshaler
{
    public function marshal($data);
}
```

Due to this simple interface/design decoration is used heavily in the marshalers. So typically, you'll use the marshaler factory to create a tree of marshalers where the root one will start the marshaling process for your data.

```php
<?php

$data = [
    [1,2],
    [3,4],
    [5,6],
    9,
    8,
];

$marshaler = $mf->collection( // $mf->map
    $mf->pipe(
        $mf->maybe(true, $mf->closure(function($item)
        {
            if (is_array($item)) {
                $item = array_sum($item);
            }

            return $item;
        })),
        $mf->closure(function($data)
        {
            return $data + 1;
        })
    )
);

print_r($marshaler->marshal($data));
```
```
[4, 8, 12, 10, 9]
```

## Leaf Marshalers vs Tree Marshalers

When you

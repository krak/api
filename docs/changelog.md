# Changelog

## 0.6.1

- Added the `Response\Factory` class for backwards compatibility with v0.4

## 0.6.0

- Removed the HMAC library
- Added the auth to the service provider
- Deprecated the Marshaler library in favor for the [Krak Marshal Library](https://gitlab.bighead.net/krak/marshal)
- Fixed minor bugs in the library
- Updated the unit tests

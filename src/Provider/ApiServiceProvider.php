<?php

namespace Krak\Api\Provider;

use Closure,
    Krak\Api\EventListener,
    Krak\Api\HttpConverter,
    Krak\Api\Marshaler,
    Krak\Api\Response,
    Krak\Api\Security,

    Pimple\Container,
    Pimple\ServiceProviderInterface,
    RuntimeException,
    StringTemplate;

class ApiServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $def = function($name, $predicate) use ($app) {
            $app['krak.api.' . $name] = $predicate;
        };

        $this->registerEventListener($def);
        $this->registerHttpConverter($def);
        $this->registerMarshaler($def);
        $this->registerResponse($def);
        $this->registerSecurity($def);
    }

    private function registerMarshaler(Closure $def)
    {
        $def('marshaler.factory', function(Container $app)
        {
            $p = 'krak.api.marshaler.factory.';

            /* handle the parameters */
            if (!isset($app[$p . 'alias_map'])) {
                $app[$p . 'alias_map'] = [];
            }
            if (!isset($app[$p . 'cache'])) {
                $app[$p . 'cache'] = null;
            }

            return new Marshaler\MarshalerFactory(
                $app[$p.'alias_map'],
                $app[$p.'cache']
            );
        });
    }

    private function registerSecurity(Closure $def)
    {
        $def('security.context', function(Container $app)
        {
            $p = 'krak.api.security.context.';
            if (!isset($app[$p.'token_factory'])) {
                $this->throwMissingParameter($p.'token_factory');
            }

            return new Security\SecurityContext(
                $app[$p.'token_factory']
            );
        });
        $def('security.firewall.request_handler', function(Container $app) use ($def)
        {
            $p = 'krak.api.security.firewall.request_handler.';

            /* handle the parameters */
            if (!isset($app[$p . 'context'])) {
                $app[$p . 'context'] = $app['krak.api.security.context'];
            }
            if (!isset($app[$p . 'firewall'])) {
                $this->throwMissingParameter($p . 'firewall');
            }
            if (!isset($app[$p . 'response_factory'])) {
                $app[$p . 'response_factory'] = $app->protect(function() {
                    return new Response\ErrorResponse('invalid access');
                });
            }
            if (!isset($app[$p . 'converter'])) {
                $app[$p . 'converter'] = $app['krak.api.http_converter.json'];
            }

            return new Security\Firewall\FirewallRequestHandler(
                $app[$p . 'context'],
                $app[$p . 'firewall'],
                $app[$p . 'response_factory'],
                $app[$p . 'converter']
            );
        });
        $def('security.auth.request_handler', function(Container $app) use ($def)
        {
            $p = 'krak.api.security.auth.request_handler.';

            /* handle the parameters */
            if (!isset($app[$p . 'context'])) {
                $app[$p . 'context'] = $app['krak.api.security.context'];
            }
            if (!isset($app[$p . 'auths'])) {
                $app[$p.'auths'] = [];
            }

            return new Security\Auth\AuthRequestHandler(
                $app[$p . 'context'],
                $app[$p . 'auths']
            );
        });
    }

    private function registerEventListener(Closure $def)
    {
        $def('event_listener.api_response_view_listener', function(Container $app) use ($def)
        {
            $p = 'krak.api.event_listener.api_response_view_listener.';

            if (!isset($app[$p . 'converter'])) {
                $app[$p . 'converter'] = $app['krak.api.http_converter.json'];
            }

            return new EventListener\ApiResponseListener(
                $app[$p . 'converter']
            );
        });
    }

    private function registerHttpConverter(Closure $def)
    {
        $def('http_converter.json', function(Container $app)
        {
            $p = 'krak.api.http_converter.json.';

            if (!isset($app[$p . 'pretty_print'])) {
                $app[$p . 'pretty_print'] = false;
            }

            return new HttpConverter\JsonHttpConverter(
                $app[$p . 'pretty_print']
            );
        });
    }

    private function registerResponse(Closure $def)
    {
        $def('response.error_code_mapping', function(Container $app)
        {
            $p = 'krak.api.response.error_code_mapping.';

            if (!isset($app[$p . 'engine'])) {
                $app[$p . 'engine'] = new StringTemplate\Engine();
            }
            if (!isset($app[$p . 'mapping'])) {
                $this->throwMissingParameter($p . 'mapping');
            }

            return new Response\ErrorCodeMapping(
                $app[$p . 'engine'],
                $app[$p . 'mapping']
            );
        });
        $def('response.factory', function(Container $app)
        {
            $p = 'krak.api.response.factory.';

            if (!isset($app[$p . 'error_code_mapping'])) {
                $app[$p . 'error_code_mapping'] = $app['krak.api.response.error_code_mapping'];
            }

            return new Response\ResponseFactory(
                $app[$p . 'error_code_mapping']
            );
        });
    }

    private function throwMissingParameter($param)
    {
        throw new RuntimeException(sprintf('%s is not set', $param));
    }

}

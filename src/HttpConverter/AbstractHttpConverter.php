<?php

namespace Krak\Api\HttpConverter;

use Krak\Api\Response\Response;
use Krak\Api\ResponseMarshaler\ResponseMarshaler;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractHttpConverter implements HttpConverter
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ResponseMarshaler
     */
    protected $marshaler;

    public function __construct(ResponseMarshaler $marshaler = null)
    {
        $this->setResponseMarshaler($marshaler);
    }

    public function setRequest(Request $req)
    {
        $this->request = $req;
    }

    public function setResponseMarshaler(ResponseMarshaler $marshaler)
    {
        $this->marshaler = $marshaler;
    }

    /**
     * Handles the actual creation of the response data
     */
    protected function createResponseData(Response $resp)
    {
        if (!$this->marshaler) {
            return $resp->createResponseData();
        }

        return $this->marshaler->marshalResponse($resp);
    }

    abstract public function toHttp(Response $resp);
}

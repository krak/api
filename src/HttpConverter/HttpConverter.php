<?php

namespace Krak\Api\HttpConverter;

use Krak\Api\Response\Response;
use Krak\Api\ResponseMarshaler\ResponseMarshaler;
use Symfony\Component\HttpFoundation\Request;

interface HttpConverter
{
    public function setRequest(Request $req);
    public function toHttp(Response $resp);
}

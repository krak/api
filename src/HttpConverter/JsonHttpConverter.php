<?php

namespace Krak\Api\HttpConverter;

use Krak\Api\Response\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonHttpConverter extends AbstractHttpConverter
{
    /**
     * @var bool
     */
    private $pretty_print;
    
    public function __construct($pretty_print = false)
    {
        $this->pretty_print = $pretty_print;
    }
    
    public function toHttp(Response $response)
    {
        $http_resp = new JsonResponse();
        
        if ($this->pretty_print) {
            $http_resp->setEncodingOptions(
                $http_resp->getEncodingOptions() | JSON_PRETTY_PRINT
            );
        }
        $http_resp->setData($this->createResponseData($response));
        
        return $http_resp;
    }
}

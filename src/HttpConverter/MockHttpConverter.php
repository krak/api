<?php

namespace Krak\Api\HttpConverter;

use Krak\Api\Response\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

/**
 * Mock Http Converter
 * Converter used for testing
 */
class MockHttpConverter extends AbstractHttpConverter
{
    /**
     * @var HttpResponse
     */
    private $http_resp;

    public function __construct(HttpResponse $resp)
    {
        $this->http_resp = $resp;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function toHttp(Response $resp)
    {
        return $this->http_resp;
    }
}

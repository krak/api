<?php

namespace Krak\Api\Response;

use Krak\Api\Marshaler\Marshaler;

class ResponseFactory
{
    private $error_code_mapping;

    public function __construct(ErrorCodeMapping $error_code_mapping)
    {
        $this->error_code_mapping = $error_code_mapping;
    }

    public function errorCode($code, $data = null)
    {
        return new ErrorCodeResponse(
            $code,
            $this->error_code_mapping->getErrorMessage($code, $data)
        );
    }

    public function ok($data, $marshaler = null)
    {
        return new OkResponse($data, $marshaler);
    }

    public function error($data)
    {
        return new ErrorResponse($data);
    }
}

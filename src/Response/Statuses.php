<?php

namespace Krak\Api\Response;

final class Statuses
{
    const OK                    = 'OK';
    const ERROR                 = 'ERROR';
    const INVALID_PARAMETERS    = 'INVALID_PARAMETERS';
}

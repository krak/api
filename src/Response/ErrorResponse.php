<?php

namespace Krak\Api\Response;

class ErrorResponse implements Response
{
    /**
     * @var mixed
     */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getStatus()
    {
        return Statuses::ERROR;
    }

    public function getData()
    {
        return $this->data;
    }

    public function createResponseData()
    {
        return [
            'error' => $this->data,
        ];
    }
}

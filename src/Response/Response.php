<?php

namespace Krak\Api\Response;

interface Response
{
    /**
     * Return the response status
     */
    public function getStatus();

    /**
     * Creates the response data to be converted into an http response
     */
    public function createResponseData();
}

<?php

namespace Krak\Api\Response;

class ErrorCodeResponse implements Response
{
    private $code;
    private $message;

    public function __construct($code, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    public function getStatus()
    {
        return Statuses::ERROR;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function createResponseData()
    {
        return [
            'error' => [
                'code' => $this->code,
                'message' => $this->message,
            ]
        ];
    }
}

<?php

namespace Krak\Api\Response;

class Factory
{
    public static function ok($data, $marshaler = null)
    {
        return new OkResponse($data, $marshaler);
    }

    public static function error($data)
    {
        return new ErrorResponse($data);
    }
}

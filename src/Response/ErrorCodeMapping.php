<?php

namespace Krak\Api\Response;

use StringTemplate\AbstractEngine;

/**
 * Map API error codes to to strings. The API formats
 * support sprintf templating via the StringTemplate Library
 */
class ErrorCodeMapping
{
    private $engine;
    private $mapping;

    public function __construct(AbstractEngine $engine, $mapping)
    {
        $this->engine = $engine;
        $this->mapping = $mapping;
    }

    public function getErrorMessage($code, $data = null)
    {
        if (!array_key_exists($code, $this->mapping)) {
            return '';
        }

        return $this->engine->render(
            $this->mapping[$code],
            $data
        );
    }
}

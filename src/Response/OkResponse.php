<?php

namespace Krak\Api\Response;

use Krak\Marshal as m;

class OkResponse implements Response
{
    /**
     * @var mixed
     */
    private $data;

    private $marshaler;

    public function __construct($data, $marshaler = null)
    {
        $this->data = $data;
        $this->marshaler = $marshaler;
    }

    public function getStatus()
    {
        return Statuses::OK;
    }

    public function getData()
    {
        return $this->data;
    }

    public function createResponseData()
    {
        return [
            'data'  => ($this->marshaler)
                ? m\marshal($this->marshaler, $this->data)
                : $this->data
        ];
    }
}

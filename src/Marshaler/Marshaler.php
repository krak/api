<?php

namespace Krak\Api\Marshaler;

use Krak\Marshal;

/**
 * Api Data Marshaler
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
interface Marshaler extends Marshal\Marshaler
{}

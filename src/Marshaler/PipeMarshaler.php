<?php

namespace Krak\Api\Marshaler;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
class PipeMarshaler implements Marshaler
{
    private $marshalers;

    public function __construct($marshalers)
    {
        $this->marshalers = $marshalers;
    }

    public function marshal($data)
    {
        foreach ($this->marshalers as $marshaler) {
            $data = $marshaler->marshal($data);
        }

        return $data;
    }
}

<?php

namespace Krak\Api\Marshaler;

use iter;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
class CollectionMarshaler extends DecoratedMarshaler
{
    public function marshal($collection)
    {
        $marshal = function($item) {
            return $this->marshaler->marshal($item);
        };

        return iter\toArray(iter\map($marshal, $collection));
    }
}

<?php

namespace Krak\Api\Marshaler;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
abstract class DecoratedMarshaler implements Marshaler
{
    protected $marhaler;

    public function __construct(Marshaler $marshaler)
    {
        $this->marshaler = $marshaler;
    }

    abstract public function marshal($data);
}

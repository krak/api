<?php

namespace Krak\Api\Marshaler;

use Krak\Marshal as m,
    DateTime;

/**
 * Convert an object to a timestamp
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
function timestamp($val)
{
    return m\timestamp($val);
}

/**
 * Optionally marshal data into an array based off the marshalers
 * passed in. This is a convenience method for helping custom marshalers
 * delegate their marshaling to other entities
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
function marshal_recurse(&$array, $data, $marshalers)
{
    foreach ($marshalers as $prop => $marshaler) {
        if (!$marshaler) {
            continue;
        }

        if (!isset($data[$prop])) {
            continue;
        }

        $array[$prop] = $marshaler->marshal($data[$prop]);
    }

    return $array;
}

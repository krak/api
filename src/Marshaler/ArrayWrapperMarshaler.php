<?php

namespace Krak\Api\Marshaler;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
class ArrayWrapperMarshaler implements Marshaler
{
    public function marshal($data)
    {
        return new ArrayWrapper($data);
    }
}

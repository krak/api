<?php

namespace Krak\Api\Marshaler;

use BadMethodCallException,
    Closure,
    Doctrine\Common\Cache,
    LogicException,
    ReflectionClass;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
class MarshalerFactory
{
    /**
     * The alias map will map a method name to the class name or closure
     *
     * @var array
     */
    private $alias_map;
    /**
     * @var Cache\Cache
     */
    private $class_cache;

    public function __construct($alias_map, Cache\Cache $cache = null)
    {
        $this->alias_map = $alias_map;
        $this->class_cache = $cache ?: new Cache\ArrayCache();
    }

    public function maybe($condition, Marshaler $marshaler)
    {
        return new MaybeMarshaler($condition, $marshaler);
    }

    public function pipe()
    {
        return new PipeMarshaler(func_get_args());
    }

    public function wrap()
    {
        return new ArrayWrapperMarshaler();
    }

    public function id()
    {
        return new IdentityMarshaler();
    }

    public function nullable(Marshaler $marshaler)
    {
        return new AllowNullMarshaler($marshaler);
    }

    public function closure(Closure $closure)
    {
        return new ClosureMarshaler($closure);
    }

    public function collection(Marshaler $marshaler)
    {
        return new CollectionMarshaler($marshaler);
    }
    /**
     * Alias for collection
     */
    public function map(Marshaler $marshaler)
    {
        return $this->collection($marshaler);
    }

    public function __call($name, $args)
    {
        if (!array_key_exists($name, $this->alias_map)) {
            throw BadMethodCallException('Invalid method call (alias not defined)');
        }

        $class = $this->alias_map[$name];

        if (is_string($class)) {
            $refl_class = $this->class_cache->fetch($class);

            if (!$refl_class) {
                $refl_class = new ReflectionClass($class);
                $this->class_cache->save($class, $refl_class);
            }

            return $refl_class->newInstanceArgs($args);
        }
        else if ($class instanceof Closure) {
            return $class($this, $args);
        }
        else {
            throw new LogicException('Alias map can only hold class names or closures that create marshalers');
        }
    }
}

<?php

namespace Krak\Api\Marshaler;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
class MockMarshaler implements Marshaler
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function marshal($other_data)
    {
        return $this->data;
    }
}

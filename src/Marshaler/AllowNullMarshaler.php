<?php

namespace Krak\Api\Marshaler;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
class AllowNullMarshaler extends DecoratedMarshaler
{
    public function marshal($data)
    {
        if ($data == null) {
            return null;
        }

        return $this->marshaler->marshal($data);
    }
}

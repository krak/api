<?php

namespace Krak\Api\Marshaler;

use ArrayAccess,
    BadMethodCallException,
    iter;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
class ArrayWrapper implements ArrayAccess
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function offsetExists($offset)
    {
        return isset($this->data->{$offset});
    }

    public function offsetGet($offset)
    {
        $res = $this->data->{$offset};

        if ($res instanceof \DateTime) {
            return $res;
        }
        if ((is_array($res) && is_int(key($res))) || $res instanceof \Traversable) {
            return iter\map(function($item)
            {
                return new ArrayWrapper($item);
            }, $res);
        }
        else if (is_object($res)) {
            return new self($res);
        }
        else {
            return $res;
        }
    }

    public function offsetSet($offset, $value)
    {
        throw new BadMethodCallException('Only read only access is allowed');
    }
    public function offsetUnset($offset)
    {
        throw new BadMethodCallException('Only read only access is allowed');
    }
}

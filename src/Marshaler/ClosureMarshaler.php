<?php

namespace Krak\Api\Marshaler;

use Closure;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
class ClosureMarshaler implements Marshaler
{
    private $predicate;

    public function __construct(Closure $predicate)
    {
        $this->predicate = $predicate;
    }

    public function marshal($data)
    {
        return $this->predicate->__invoke($data);
    }
}

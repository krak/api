<?php

namespace Krak\Api\Marshaler;

/**
 * @deprecated 0.6.0 This is being replaced in favor for the Krak\Marhal library
 */
class MaybeMarshaler implements Marshaler
{
    private $marshaler;

    public function __construct($condition, $marshaler)
    {
        $this->marshaler = ($condition) ? $marshaler : new IdentityMarshaler();
    }

    public function marshal($data)
    {
        return $this->marshaler->marshal($data);
    }
}

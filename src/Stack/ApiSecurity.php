<?php

namespace Krak\Api\Stack;

use Krak\Api\Security,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpKernel\HttpKernelInterface;

class ApiSecurity implements HttpKernelInterface
{
    private $app;
    private $auth_handler;
    private $firewall_handler;

    public function __construct(
        HttpKernelInterface $app,
        Security\Auth\AuthRequestHandler $auth_handler,
        Security\Firewall\FirewallRequestHandler $firewall_handler = null
    ) {
        $this->app = $app;
        $this->auth_handler = $auth_handler;
        $this->firewall_handler = $firewall_handler;
    }

    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true) {
        /* for any sub requests, just delegate */
        if ($type != HttpKernelInterface::MASTER_REQUEST) {
            return $this->app->handle($request, $type, $catch);
        }

        $this->auth_handler->handleRequest($request);

        $resp = ($this->firewall_handler)
            ? $this->firewall_handler->handleRequest($request)
            : null;

        if (!$resp) {
            return $this->app->handle($request, $type, $catch);
        }

        return $resp;
    }
}

<?php

namespace Krak\Api\Security\Token;

interface TokenFactory
{
    public function createToken();
}

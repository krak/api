<?php

namespace Krak\Api\Security\Token;

class MockTokenFactory implements TokenFactory
{
    private $token;

    public function __construct(Token $tok)
    {
        $this->token = $tok;
    }

    public function createToken()
    {
        return $this->token;
    }
}

<?php

namespace Krak\Api\Security\Token;

trait ApiClientTokenTrait
{
    protected $api_client_id;

    public function getApiClientId()
    {
        return $this->api_client_id;
    }
    public function setApiClientId($key)
    {
        $this->api_client_id = $key;
    }
}

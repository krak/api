<?php

namespace Krak\Api\Security\Token;

interface UserToken extends Token
{
    public function getUserId();
    public function setUserId($id);
}

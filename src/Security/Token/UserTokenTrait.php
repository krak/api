<?php

namespace Krak\Api\Security\Token;

trait UserTokenTrait
{
    protected $user_id;

    public function getUserId()
    {
        return $this->user_id;
    }
    public function setUserId($id)
    {
        $this->user_id = $id;
    }
}

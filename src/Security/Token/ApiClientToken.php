<?php

namespace Krak\Api\Security\Token;

interface ApiClientToken extends Token
{
    public function getApiClientId();
    public function setApiClientId($key);
}

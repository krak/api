<?php

namespace Krak\Api\Security\Token;

/**
 * Abstract Token Decorator
 * Base class to help implement token decorators
 */
abstract class AbstractTokenDecorator implements Token
{
    protected $token;

    public function __construct(Token $tok)
    {
        $this->token = $tok;
    }

    public function revokePermission($permission)
    {
        $this->token->revokePermission($permission);
    }

    public function addPermission($permission)
    {
        $this->token->addPermission($permission);
    }

    public function hasPermission($permission)
    {
        return $this->token->hasPermission($permission);
    }

    public function getAllPermissions()
    {
        return $this->token->getAllPermissions();
    }
}

<?php

namespace Krak\Api\Security\Token;

/**
 * Security Token
 * Represents the authorization and data for the application
 */
interface Token
{
    /**
     * Revoke a permission from this token
     * @param string $permission
     */
    public function revokePermission($permission);

    /**
     * Add a permission for this token
     * @param string $permission
     */
    public function addPermission($permission);

    /**
     * Whether or not this token has a certain permission
     * @param string $permission
     * @return bool
     */
    public function hasPermission($permission);

    /**
     * Return all of the permissions for this token
     * @return array
     */
    public function getAllPermissions();
}

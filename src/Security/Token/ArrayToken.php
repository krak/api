<?php

namespace Krak\Api\Security\Token;

/**
 * Array Token
 * This token implementation uses a php for storage
 */
class ArrayToken implements Token
{
    /**
     * a set of permissions
     * @var array
     */
    private $permissions;

    public function __construct()
    {
        $this->permissions = [];
    }

    /**
     * @inheritDoc
     */
    public function revokePermission($permission)
    {
        unset($this->permissions[$permission]);
    }

    /**
     * @inheritDoc
     */
    public function addPermission($permission)
    {
        $this->permissions[$permission] = null;
    }

    /**
     * @inheritDoc
     */
    public function hasPermission($permission)
    {
        return array_key_exists($permission, $this->permissions);
    }

    /**
     * @inheritDoc
     */
    public function getAllPermissions()
    {
        return array_keys($this->permissions);
    }
}

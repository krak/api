<?php

namespace Krak\Api\Security\Firewall;

use Krak\Api\Security\Token\Token,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\RequestMatcherInterface;

class RequestMatcherFirewall implements Firewall
{
    /**
     * @var RequestMatcherInterface
     */
    public $matcher;
    /**
     * @var array
     */
    public $permissions;

    public function __construct(RequestMatcherInterface $matcher, $permissions)
    {
        $this->matcher = $matcher;
        $this->permissions = $permissions;
    }

    public function allowRequest(Request $request, Token $tok)
    {
        /* if we don't have a match for this request, then we let it through */
        if (!$this->matcher->matches($request)) {
            return true;
        }

        foreach ($this->permissions as $permission) {
            if (!$tok->hasPermission($permission)) {
                return false;
            }
        }

        return true;
    }
}

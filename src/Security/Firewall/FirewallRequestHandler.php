<?php

namespace Krak\Api\Security\Firewall;

use Closure,
    Krak\Api\HttpConverter\HttpConverter,
    Krak\Api\Security\SecurityContext,
    Symfony\Component\HttpFoundation;

class FirewallRequestHandler
{
    private $context;
    private $firewall;
    private $response_factory;
    private $converter;

    /**
     * @param SecurityContext $context
     * @param Firewall $firewall Used to test the security context token
     * @param Closure $response_factory this will return an instance of \Krak\Api\Response\Response
     * @param HttpConverter $converter Converts the ApiResponse into an actual Http Symfony Response
     */
    public function __construct(
        SecurityContext $context,
        Firewall $firewall,
        Closure $response_factory,
        HttpConverter $converter
    ) {
        $this->context = $context;
        $this->firewall = $firewall;
        $this->response_factory = $response_factory;
        $this->converter = $converter;
    }

    /**
     * returns null on allow or the response on block
     * @return null|HttpFoundation\Response
     */
    public function handleRequest(HttpFoundation\Request $request)
    {
        $res = $this->firewall->allowRequest(
            $request,
            $this->context->getToken()
        );

        if ($res) {
            return; /* allows the request */
        }

        $this->converter->setRequest($request);

        return $this->converter->toHttp($this->response_factory->__invoke());
    }
}

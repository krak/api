<?php

namespace Krak\Api\Security\Firewall;

use Krak\Api\Security\Token\Token,
    Symfony\Component\HttpFoundation\Request;

/**
 * Firewall
 * A firewall accepts a request and token and decides on whether or not
 * to allow the request through
 */
interface Firewall
{
    public function allowRequest(Request $request, Token $tok);
}

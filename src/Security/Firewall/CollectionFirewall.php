<?php

namespace Krak\Api\Security\Firewall;

use Krak\Api\Security\Token\Token,
    Symfony\Component\HttpFoundation\Request;

class CollectionFirewall implements Firewall
{
    private $firewalls;

    public function __construct($firewalls)
    {
        $this->firewalls = $firewalls;
    }

    public function allowRequest(Request $request, Token $tok)
    {
        foreach ($this->firewalls as $firewall) {
            if (!$firewall->allowRequest($request, $tok)) {
                return false;
            }
        }

        return true;
    }
}

<?php

namespace Krak\Api\Security\Firewall;

use Krak\Api\Security\Token,
    Symfony\Component\HttpFoundation\Request;

class StaticFirewall implements Firewall
{
    private $is_open;

    public function __construct($is_open)
    {
        $this->is_open = $is_open;
    }

    public function allowRequest(Request $req, Token\Token $token)
    {
        return $this->is_open;
    }
}

<?php

namespace Krak\Api\Security\Auth;

use Symfony\Component\HttpFoundation\Request;

interface Auth
{
    /**
     * Authenticate a given set of credentials
     * @return PermissionGranter
     */
    public function authenticate(Request $request);
}

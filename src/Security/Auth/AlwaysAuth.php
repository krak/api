<?php

namespace Krak\Api\Security\Auth;

use Krak\Api\Security\PermissionGranter,
    Symfony\Component\HttpFoundation\Request;

/**
 * AlwaysAuth will always authenticate a request and return the passed
 * in granter. This is great for unit testing
 */
class AlwaysAuth implements Auth
{
    private $granter;

    public function __construct(PermissionGranter\PermissionGranter $granter)
    {
        $this->granter = $granter;
    }

    public function authenticate(Request $req)
    {
        return $this->granter;
    }
}

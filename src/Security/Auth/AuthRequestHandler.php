<?php

namespace Krak\Api\Security\Auth;

use Krak\Api\Security,
    Symfony\Component\HttpFoundation\Request;

/**
 * AuthenticatorHandler
 * Authenticates the credentials and grants permission to the token
 */
class AuthRequestHandler
{
    private $context;
    private $auths;

    public function __construct(Security\SecurityContext $context, $auth)
    {
        $this->context = $context;
        $this->auths = $auth;
    }

    public function handleRequest(Request $request)
    {
        foreach ($this->auths as $auth) {
            $auth->authenticate($request)
                ->grantPermission($this->context->getToken());
        }
    }
}

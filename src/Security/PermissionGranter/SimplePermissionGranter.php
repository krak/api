<?php

namespace Krak\Api\Security\PermissionGranter;

use Krak\Api\Security\Token;

class SimplePermissionGranter implements PermissionGranter
{
    private $permissions;

    public function __construct($permissions)
    {
        $this->permissions = $permissions;
    }

    public function grantPermission(Token\Token $tok)
    {
        foreach ($this->permissions as $permission) {
            $tok->addPermission($permission);
        }
    }
}

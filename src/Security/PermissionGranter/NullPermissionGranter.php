<?php

namespace Krak\Api\Security\PermissionGranter;

use Krak\Api\Security\Token\Token;

/**
 * Null Permission Granter
 * Dummy granter that won't doesn't grant any permissions
 * for a token
 */
class NullPermissionGranter implements PermissionGranter
{
    /**
     * Don't grant this token anything
     */
    public function grantPermission(Token $tok)
    {
        return;
    }
}

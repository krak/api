<?php

namespace Krak\Api\Security\PermissionGranter;

use Krak\Api\Security\Token\Token;

interface PermissionGranter
{
    /**
     * grant/revoke permissions on a token
     */
    public function grantPermission(Token $tok);
}

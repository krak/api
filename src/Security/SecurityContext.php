<?php

namespace Krak\Api\Security;

/**
 * Security Context
 * Holds the context for security/state
 */
class SecurityContext
{
    private $token;
    private $token_factory;

    public function __construct(Token\TokenFactory $factory)
    {
        $this->token = $factory->createToken();
        $this->token_factory = $factory;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getTokenFactory()
    {
        return $this->token_factory;
    }
}

<?php

namespace Krak\Api;

use Symfony\Component\HttpFoundation\Request;

function request_json(Request $req, $raw = false)
{
    $is_json = $req->getContentType() == 'json';

    if ($raw && !$is_json) {
        return null;
    }
    else if ($raw && $is_json) {
        return json_decode($req->getContent(), true);
    }
    else if (!$raw && !$is_json) {
        return [];
    }
    else { // (!$raw && $is_json)
        $data = json_decode($req->getContent(), true);

        if (!is_array($data)) {
            return [];
        }

        return $data;
    }
}

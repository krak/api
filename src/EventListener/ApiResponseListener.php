<?php

namespace Krak\Api\EventListener;

use Krak\Api\HttpConverter\HttpConverter,
    Krak\Api\Response\Response,

    Symfony\Component\EventDispatcher\EventSubscriberInterface,
    Symfony\Component\HttpKernel\KernelEvents,
    Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class ApiResponseListener implements EventSubscriberInterface
{
    /**
     * @var HttpConverter
     */
    private $converter;

    public function __construct(HttpConverter $converter)
    {
        $this->converter = $converter;
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $api_resp = $event->getControllerResult();

        if ($api_resp instanceof Response == false) {
            return; /* nothing to do, this isn't an api response */
        }

        $this->converter->setRequest($event->getRequest());
        $event->setResponse($this->converter->toHttp($api_resp));
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['onKernelView', 0],
        ];
    }
}

<?php

namespace Krak\Api\ResponseMarshaler;

use Krak\Api\Response\Response;

interface ResponseMarshaler
{
    public function marshalResponse(Response $resp);
}

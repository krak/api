<?php

namespace Krak\Api\ResponseMarshaler;

use Krak\Api\Response\Response;

class StatusResponseMarshaler implements ResponseMarshaler
{
    public function marshalResponse(Response $resp)
    {
        return [
            'status' => $resp->getStatus()
        ] + $resp->createResponseData();
    }
}
